package com.example.redweb.currentlocation;

import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.nio.charset.CodingErrorAction;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MyActivity extends Activity{
    private final static int PICKUP_CODE =1;
    private final static int TAKEPICTURE_CODE =2;
    TextView textView;
    Button btnPickUp, btnTakePicture;
    Uri selectedUri;
    Uri nameUri;
    String imagePath;
    String nameOfImage;
    long length;
    String size;
    ImageView ivShow;

    String printDate;
    String[] proj = new String[]{
            MediaStore.Images.ImageColumns._ID,
            MediaStore.Images.ImageColumns.DATA,
            MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
            MediaStore.Images.ImageColumns.DATE_TAKEN,
            MediaStore.Images.ImageColumns.MIME_TYPE,
            MediaStore.Images.ImageColumns.DISPLAY_NAME,
            MediaStore.Images.ImageColumns.SIZE
    };

    //use to find out size of image
    File file;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        ivShow = (ImageView)findViewById(R.id.ivshow);
        btnPickUp = (Button)findViewById(R.id.button);
        btnTakePicture = (Button)findViewById(R.id.button2);
        textView = (TextView)findViewById(R.id.textview);
        btnTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),TAKEPICTURE_CODE);
            }
        });
        btnPickUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI),PICKUP_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case PICKUP_CODE:
                    selectedUri = data.getData();
                    getRealPathFromURIVoid(this,selectedUri,proj,null);
                    print();
                    break;
                case TAKEPICTURE_CODE:
                     getRealPathFromURIVoid(this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, proj,MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
                    print();
                    break;
                default:
                    break;
            }
        }
    }

    public void print()
    {
        textView.setText( "name: " + nameOfImage + "/n Date: " + printDate+ " -size " + size);
    }

    public void getRealPathFromURIVoid(Context context, Uri contentUri, String[] projection,String sort) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(contentUri, projection, null, null,sort);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            if(cursor.moveToFirst()) {
                //it holds image.

                imagePath = cursor.getString(column_index);
                DateFormat dateFormat = new SimpleDateFormat("MMMM dd yyyy");
                printDate = dateFormat.format(new Date(cursor.getLong(3)));
                nameOfImage = cursor.getString(5);
                size = cursor.getString(6);


                //can replace  int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA); and
                // imagePath = cursor.getString(column_index);
                //by imagepath = cursor.getString(1);
                //in case of displaying image in view(imageview or whatever)
                 Bitmap yourSelectedImage = BitmapFactory.decodeFile(imagePath);
                ivShow.setImageBitmap(yourSelectedImage);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }




    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = new String[]{
                    MediaStore.Images.ImageColumns._ID,
                    MediaStore.Images.ImageColumns.DATA,
                    MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                    MediaStore.Images.ImageColumns.DATE_TAKEN,
                    MediaStore.Images.ImageColumns.MIME_TYPE
            };
            //  String[] proj = { MediaStore.Images.Media.DATA };


            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
    public String pathOfLastImage()
    {
        String imageLocation=null;
        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE,
                MediaStore.Images.ImageColumns.SIZE
        };
        final Cursor cursor = getContentResolver()
                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                        null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

// Put it in the image view
        if (cursor.moveToFirst()) {
            //  final ImageView imageView = (ImageView) findViewById(R.id.pictureView);

            imageLocation = cursor.getString(1);
            long dateml = cursor.getLong(3);
            Date date = new Date(dateml);
            printDate = date.toString();
//            File imageFile = new File(imageLocation);
//            if (imageFile.exists()) {   // TODO: is there a better way to do this?
//                Bitmap bm = BitmapFactory.decodeFile(imageLocation);
//                imageView.setImageBitmap(bm);
//            }
        }
        return imageLocation;
    }
    private void startAction(String action, int requestCode)
    {
        Intent intent = new Intent(action,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, requestCode);
    }
}
